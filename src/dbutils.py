"""Useful stuff stays here,
until it grows enough to need
its own file/module.
"""
import logging
from datetime import timedelta

import numpy as np
import pandas as pd

from .config import USERNAME, PSQL_PASSWD, PSQL_HOST, PSQL_PORT, DB_NAME
from sqlalchemy import create_engine, MetaData, select, insert, func

logger = logging.getLogger()


def render_passwd(code):
    """Poor man's decryption of codes"""
    passwd = "".join([x[0] for x in code.split(" ")])
    return passwd


def db_engine_setup():
    """Setup DB engine

    The returned Engine object can be used to established
    a connection through a context manager using
    the .begin() method.
    The standard syntax is dialect+driver://username:password@host:port/database
    This sets up a PostgreSQL DB using the psycopg2 driver.

    :return: SQLAlchemy Engine instance
    """
    psql_password = render_passwd(PSQL_PASSWD)
    engine = create_engine(
        "postgresql+psycopg2://{}:{}@{}:{}/{}".format(
            USERNAME, psql_password, PSQL_HOST, PSQL_PORT, DB_NAME
        ),
        echo=True,
    )
    return engine


def db_reflect_metadata(engine):
    """Reflect the existing database metadata
    The returned object contains all information
    on database tables and columns.

    :param engine: SQLAlchemy Engine instance
    :return: MetaData instance
    """
    metadata = MetaData(engine)
    metadata.reflect(bind=engine)
    return metadata


def db_get_users_views(users_table, engine):
    """Fetch all user ids and respective view count from DB

    :param users_table: A SQLAlchemy Table object
    :param engine: A SQLAlchemy Engine object
    :return:
    user_views: dict
    """
    with engine.begin() as conn:
        result = conn.execute(select([users_table.c.id, users_table.c.view_count]))
        users_views = {x.id: x.view_count for x in result}
        # user_id_set = {x.id for x in result}
    return users_views


def db_get_top_users_views(users_table, engine, top_limit=10):
    """Fetch top user ids and respective view count from DB,
    ordered by view count

    :param top_limit: int
    :param users_table: A SQLAlchemy Table object
    :param engine: A SQLAlchemy Engine object
    :return:
    user_views: dict
    """
    with engine.begin() as conn:
        result = conn.execute(
            select([users_table.c.id, users_table.c.view_count])
            .order_by(users_table.c.view_count.desc())
            .limit(top_limit)
        )
        users_views = {x.id: x.view_count for x in result}
        # user_id_set = {x.id for x in result}
    return users_views


def db_insert_users(db_engine, new_users, users_table):
    """Insert list of users to the users table in the DB

    :param db_engine: SQLAlchemy engine object
    :param new_users: iterable
    :param users_table: SQLAlchemy table object
    :return: None
    """
    logger.debug("Inserting {} users to DB...".format(len(new_users)))
    with db_engine.begin() as conn:
        try:
            result = conn.execute(insert(users_table, new_users))
        except Exception as e:
            logger.debug("Something went wrong inserting users:")
            logger.error(e)
            raise


def db_insert_views_follows(db_engine, users_views_followers, views_follows_table):
    # logger.debug("Inserting {} users to DB...".format(len(new_users)))
    with db_engine.begin() as conn:
        try:
            result = conn.execute(insert(views_follows_table, users_views_followers))
        except Exception as e:
            logger.debug("Something went wrong inserting users:")
            logger.error(e)
            raise


def db_components():
    # global db_engine, users_table, views_follows_table
    logger.debug("DB Setup...")
    db_engine = db_engine_setup()
    metadata = db_reflect_metadata(db_engine)
    logger.debug(metadata.tables.keys())
    # users_table = metadata.tables['users']
    # views_follows_table = metadata.tables['views_follows']
    tables = tuple(metadata.tables[k] for k in metadata.tables.keys())
    return db_engine, tables


def get_df_top_follows(users_table, views_follows_table, db_engine, top_limit=50):
    df_follows = pd.read_sql(
        select(
            [
                users_table.c.display_name,
                views_follows_table.c.follows_count,
                views_follows_table.c.view_count,
            ]
        )
        .order_by(views_follows_table.c.query_tstamp.desc())
        .order_by(views_follows_table.c.follows_count.desc())
        .limit(top_limit)
        .select_from(
            views_follows_table.join(
                users_table, users_table.c.id == views_follows_table.c.user_id
            )
        ),
        db_engine,
    )
    return df_follows


def get_df_top_views(users_table, views_follows_table, db_engine, top_limit=50):
    df_views = pd.read_sql(
        select([users_table.c.display_name, views_follows_table.c.view_count,])
        .order_by(views_follows_table.c.query_tstamp.desc())
        .order_by(views_follows_table.c.view_count.desc())
        .limit(top_limit)
        .select_from(
            views_follows_table.join(
                users_table, users_table.c.id == views_follows_table.c.user_id
            )
        ),
        db_engine,
    )
    return df_views


def get_df_timeline(users_table, views_follows_table, db_engine):
    """Get last 2 months of data aggregated per day"""
    with db_engine.connect() as conn:
        max_date = conn.execute(
            "SELECT max(query_tstamp) as max_date FROM views_follows;"
        ).fetchone()["max_date"]

    time_series_table = (
        select(
            [
                views_follows_table.c.user_id,
                func.date_trunc("day", views_follows_table.c.query_tstamp).label(
                    "datetime_day"
                ),
                func.max(views_follows_table.c.follows_count).label("follows"),
                func.max(views_follows_table.c.view_count).label("views"),
            ]
        )
        .where(views_follows_table.c.query_tstamp > (max_date - timedelta(days=60)))
        .group_by(
            views_follows_table.c.user_id,
            func.date_trunc("day", views_follows_table.c.query_tstamp),
        )
        .alias()
    )

    mini_users_table = select([users_table.c.id, users_table.c.display_name]).alias()

    timeseries_query = (
        select(
            [
                time_series_table.c.datetime_day,
                mini_users_table.c.display_name,
                time_series_table.c.views,
                time_series_table.c.follows,
            ]
        )
        .select_from(
            time_series_table.join(
                mini_users_table, mini_users_table.c.id == time_series_table.c.user_id
            )
        )
        .order_by(time_series_table.c.datetime_day)
    )

    df_time = pd.read_sql(timeseries_query, db_engine)

    return df_time


def get_db_tables(tables):
    try:
        for t in tables:
            if t.name == "users":
                users_table = t
                continue
            if t.name == "views_follows":
                views_follows_table = t
                continue
        return users_table, views_follows_table
    except Exception as e:
        logger.error(f"Could not load users/views-follows tables: {e}")
        return None, None


def get_db_components():
    try:
        db_engine, tables = db_components()
        return db_engine, tables
    except Exception as e:
        logger.error(f"Could not load users/views-follows tables: {e}")
        return None, None


def get_dataframes(users_table, views_follows_table, db_engine):
    try:
        df_timeline = get_df_timeline(users_table, views_follows_table, db_engine)
        df_follows = (
            df_timeline.groupby("display_name")
            .last()
            .sort_values("follows", ascending=False,)
            .reset_index()
        )
        df_follows["Rank"] = df_follows.index
        df_follows = df_follows.reindex(
            columns=["Rank", "display_name", "follows", "views"]
        )

        df_views = (
            df_timeline.groupby("display_name")
            .last()
            .sort_values("views", ascending=False,)
            .reset_index()
        )
    except Exception as e:
        logger.error(f"Could not process view/users tables: {e}")
        df_follows = pd.DataFrame(
            data={
                "Rank": [np.nan],
                "display_name": [np.nan],
                "follows": [np.nan],
                "views": [np.nan],
            }
        )
        df_timeline = pd.DataFrame(
            data={
                "datetime_day": [np.nan],
                "display_name": [np.nan],
                "views": [np.nan],
                "follows": [np.nan],
            }
        )
        df_views = pd.DataFrame(data={"display_name": [np.nan], "views": [np.nan]})
    return df_timeline, df_follows, df_views


if __name__ == "__main__":

    engine = db_engine_setup()
    metadata = db_reflect_metadata(engine)
    print(metadata.tables.keys())

    users_table = metadata.tables["users"]
    views_follows_table = metadata.tables["views_follows"]
