import dash_core_components as dcc
import dash_html_components as html
import logging
import pandas as pd
from plotly import graph_objs as go

from src.config import DEFAULT_LAYOUT_KWARGS

logger = logging.getLogger(__name__)


def generate_table(dataframe: pd.DataFrame, max_rows=100):
    """Generate HTML table for Dash layout from dataframe"""
    try:
        return html.Table(
            # Header
            [html.Tr([html.Th(col) for col in dataframe.columns])] +
            # Body
            [
                html.Tr([html.Td(dataframe.iloc[i][col]) for col in dataframe.columns])
                for i in range(min(len(dataframe), max_rows))
            ],
            className="u-full-width",
        )
    except Exception as e:
        logger.error(f"Something went wrong: {e}")
        return html.Table([])


def generate_time_graph(df_time, df_follows):
    layout = go.Layout(DEFAULT_LAYOUT_KWARGS)
    layout.update(
        title="Follows Timeline", xaxis=dict(rangeslider=dict(), type="date",),
    ),
    try:
        return dcc.Graph(
            id="timeline",
            figure=dict(
                data=[
                    go.Scatter(
                        x=df_time.loc[
                            df_time["display_name"] == disp_name, "datetime_day"
                        ],
                        y=df_time.loc[df_time["display_name"] == disp_name, "follows"],
                        name=disp_name,
                        opacity=0.8,
                    )
                    for disp_name in df_follows["display_name"][:10].values
                ],
                layout=layout,
            ),
        )
    except Exception as e:
        logger.error(f"Something went wrong: {e}")
        return dcc.Graph(id="timeline", figure=dict(data=[], layout=layout,),)


def generate_top_follows_graph(df_follows):
    layout = go.Layout(DEFAULT_LAYOUT_KWARGS)
    layout.update(
        title="Top Follows", xaxis=dict(tickangle=-45,), margin=dict(b=65),
    )
    try:
        return dcc.Graph(
            id="life-exp-vs-gdp",
            figure=dict(
                data=[
                    go.Bar(x=df_follows["display_name"], y=df_follows["follows_count"],)
                ],
                layout=layout,
            ),
        )
    except Exception as e:
        logger.error(f"Something went wrong: {e}")
        return dcc.Graph(id="life-exp-vs-gdp", figure=dict(data=[], layout=layout,),)


def generate_trending_graph(df_time):
    layout = go.Layout(DEFAULT_LAYOUT_KWARGS)
    layout.update(
        title="Hottest Streamers of the month",
        xaxis=dict(type="log", autorange=True, title="Follows/day"),
        #     yaxis=dict(
        #         type='log',
        #         autorange=True
        #     ),
        margin=dict(l=115, r=35, b=35, t=35),
    )
    try:
        tot_follows = (
            df_time[
                df_time["datetime_day"]
                >= (df_time["datetime_day"].max() - pd.to_timedelta(30, unit="D"))
            ]
            .groupby("display_name")["follows"]
            .agg(["first", "last"])
            .eval("last-first")
        )
        max_days = df_time[
            df_time["datetime_day"]
            >= (df_time["datetime_day"].max() - pd.to_timedelta(30, unit="D"))
        ].groupby("display_name")["datetime_day"].agg(["min", "max"]).eval(
            "max-min"
        ) + pd.to_timedelta(
            "1 day"
        )
        df_trending = (tot_follows / max_days.dt.days).sort_values().tail(20)
        trace0 = go.Bar(
            x=df_trending,
            y=df_trending.index,
            #     marker=dict(
            #         color='rgba(50, 171, 96, 0.6)',
            #         line=dict(
            #             color='rgba(50, 171, 96, 1.0)',
            #             width=1),
            #     ),
            name="Hottest Streamers",
            orientation="h",
        )
        return dcc.Graph(
            id="trending-streamers", figure=dict(data=[trace0,], layout=layout,),
        )
    except Exception as e:
        logger.error(f"Something went wrong: {e}")
        return dcc.Graph(id="trending-streamers", figure=dict(data=[], layout=layout,),)


def generate_growth_graph(colname, user_names, df_timeline):
    df_increments = [
        df_timeline.loc[
            df_timeline.display_name == name, ["datetime_day", "follows", "views"]
        ]
        .set_index("datetime_day")
        .resample("D")
        .last()
        .interpolate()
        .diff()
        .rolling("7D")
        .mean()
        for name in user_names
    ]
    data = [
        go.Scatter(
            x=df.index,
            y=df[colname],
            name=name,
            # mode='lines+markers',
        )
        for df, name in zip(df_increments, user_names)
    ]
    layout = go.Layout(DEFAULT_LAYOUT_KWARGS)
    layout.update(
        xaxis=dict(rangeslider=dict(), type="date",), title=colname + " timeline",
    ),
    return dcc.Graph(id="ggraph", figure={"data": data, "layout": layout})
