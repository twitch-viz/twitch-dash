import os

DEFAULT_LAYOUT_KWARGS = dict(
    autosize=True,
    height=500,
    # font=dict(color='#CCCCCC'),
    # titlefont=dict(color='#CCCCCC', size='14'),
    # plot_bgcolor="#191A1A",
    # paper_bgcolor="#020202",
    # plot_bgcolor="rgba(0,0,0,0.10)",
    # paper_bgcolor="rgba(0,0,0,0.05)",
    margin=dict(l=35, r=35, b=35, t=35),
    legend={"x": 0},
    # hovermode='compare',
    # legend=dict(font=dict(size=10), orientation='h'),
    # title='Satellite Overview',
    # mapbox=dict(
    #     accesstoken=mapbox_access_token,
    #     style="dark",
    #     center=dict(
    #         lon=-78.05,
    #         lat=42.54
    #     ),
    #     zoom=7,
    # )
)
# DB CONFIG
try:
    # This is in case the dokku postgres plugin is used
    DATABASE_URL = os.environ["DATABASE_URL"]
    _, my_stuff = DATABASE_URL.split("//")
    user_pw, host_port_db = my_stuff.split("@")
    USERNAME, pw_prerender = user_pw.split(":")
    PSQL_PASSWD = " ".join(pw_prerender)
    host_port, DB_NAME = host_port_db.split("/")
    PSQL_HOST, PSQL_PORT = host_port.split(":")
except:
    USERNAME = os.environ.get("TDB_USERNAME", "postgres")
    # UNIX_PASSWD = os.environ['TDB_UNIX_PASSWD']
    PSQL_PASSWD = os.environ.get("PSQL_PASSWD", "postgres")
    PSQL_HOST = os.environ.get("PSQL_HOST", "0.0.0.0")
    PSQL_PORT = os.environ.get("PSQL_PORT", 5432)
    DB_NAME = os.environ.get("DB_NAME", "postgres")

# OTHER SETTINGS
# How many users to track
N_TOP_USERS = 100


if __name__ == "__main__":
    print(USERNAME)
