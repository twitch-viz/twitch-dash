FROM python:3.8-slim-buster

RUN useradd dash

WORKDIR /home/dash

# Grab requirements.txt.
COPY requirements.txt .
# Install dependencies
RUN python -m pip install -r requirements.txt --no-cache-dir

COPY app.py .
COPY src src
COPY start.sh .

ENV TDB_USERNAME postgres
ENV DB_NAME postgres
# TODO: find a way to set this up more elegantly, will break very easily
# ENV PSQL_HOST 172.17.0.10
ENV PSQL_PASSWD a s d a s d
# Give permission to user
RUN chown -R dash:dash ./
USER dash

EXPOSE 5431
EXPOSE 5000

CMD ["./start.sh"]
