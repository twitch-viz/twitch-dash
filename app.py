import dash
import dash_core_components as dcc
import dash_html_components as html
import logging

from src.components import (
    generate_table,
    generate_time_graph,
    generate_trending_graph,
    generate_growth_graph,
)
from src.dbutils import get_db_components, get_db_tables, get_dataframes

logger = logging.getLogger()

db_engine, db_tables = get_db_components()

users_table, views_follows_table = get_db_tables(db_tables)

df_timeline, df_follows, df_views = get_dataframes(
    users_table, views_follows_table, db_engine
)

# DECLARE APP
app = dash.Dash(serve_locally=False)
server = app.server
# Remove the following in production
# app.css.config.serve_locally = True
# app.scripts.config.serve_locally = True

app.css.append_css(
    {
        "external_url": (
            "https://cdn.rawgit.com/plotly/dash-app-stylesheets/"
            "2d266c578d2a6e8850ebce48fdb52759b2aef506/stylesheet-oil-and-gas.css"
        )
    }
)


# ### === ~~~ LAYOUT ~~~ === ###


app.layout = html.Div(
    className="container",
    children=[
        html.Div(children=html.H1("Twitch explorer"),),
        html.Div(
            [
                html.P(
                    [
                        "Source code available at ",
                        html.A(
                            "https://gitlab.com/twitch-viz/twitch-dash",
                            href="https://gitlab.com/twitch-viz/twitch-dash",
                        ),
                    ]
                ),
                html.Div(
                    dcc.RadioItems(
                        id="views-follows",
                        options=[
                            {"label": k, "value": v}
                            for k, v in {
                                "Follows/day": "follows",
                                "Views/day": "views",
                            }.items()
                        ],
                        value="follows",
                        labelStyle={"display": "inline-block"},
                    ),
                    className="three columns",
                ),
                html.Div(
                    dcc.Dropdown(
                        id="stock-ticker-input",
                        options=[
                            {"label": s, "value": s} for s in df_follows.display_name
                        ],
                        value=["summit1g", "shroud", "DrDisRespectLIVE"],
                        multi=True,
                    ),
                    className="nine columns",
                ),
            ],
            className="row",
        ),
        html.Div(id="growth-graph"),
        html.Div(
            className="row",
            children=[
                # html.Div(
                #     className='six columns',
                #     children=[
                #         generate_top_follows_graph(df_follows[df_follows['follows_count']>=1E6]),
                #     ],
                # ),
                html.Div(
                    className="six columns",
                    children=[generate_trending_graph(df_timeline),],
                ),
                # html.Div(
                #     className='six columns',
                #     children=[
                #         generate_top_follows_graph(df_follows[df_follows['follows_count']>=1E6]),
                #     ],
                # ),
                html.Div(
                    className="six columns",
                    children=[generate_time_graph(df_timeline, df_follows),],
                ),
            ],
        ),
        html.Div(
            className="row",
            children=[
                html.Div(
                    [
                        html.H3(
                            "Top followed",
                            style={"text-align": "center"},
                            className="eight columns",
                        ),
                        generate_table(df_follows),
                    ],
                    className="eight columns",
                ),
                html.Div(
                    [
                        html.H3(
                            "Top viewed",
                            style={"text-align": "center"},
                            className="four columns",
                        ),
                        generate_table(df_views),
                    ],
                    className="four columns",
                ),
            ],
        ),
    ],
)


@app.callback(
    dash.dependencies.Output("growth-graph", "children"),
    [
        dash.dependencies.Input("stock-ticker-input", "value"),
        dash.dependencies.Input("views-follows", "value"),
    ],
)
def make_increment_graph(user_names, colname="follows", df_timeline=df_timeline):
    return generate_growth_graph(colname, user_names, df_timeline)


if __name__ == "__main__":

    logging.basicConfig(
        format="%(asctime)s - %(levelname)s:%(message)s", level=logging.DEBUG,
    )
    app.run_server(debug=True)
