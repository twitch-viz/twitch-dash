# Twitch-dash
## Visualize twitch data interactively

**Note: this is a work in progress**

This is a web app for the visualization of data
from www.twitch.tv,
collected using their public API.
The _Dash_ framework by _Plotly.js_ 
is the core technology used.
See more at 
[https://plot.ly/products/dash/](https://plot.ly/products/dash/).
